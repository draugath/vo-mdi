mdi.ui = {windows = {}}

local xres = gkinterface.GetXResolution()

local function CreateOptionsButton()
	local hbox_list = {}
	
	mdi.ui.OptionsButton = iup.stationbutton{title = "MDI", size = button_size}

	function mdi.ui.OptionsButton:action()
		test.print('OptionsButton:action()')
		HideDialog(OptionsDialog)
		ShowDialog(mdi.ui.MDIOptionsDialog)
	end

	local options_vbox = OptionsDialog[1][1][1]
	local cur_hbox = iup.GetNextChild(options_vbox)
	
	while cur_hbox do
		table.insert(hbox_list, 1, cur_hbox)
		cur_hbox = iup.GetBrother(cur_hbox)
	end
	
	local button_size = hbox_list[1][1].size
	mdi.ui.OptionsButton.size = button_size

	iup.Detach(hbox_list[2]) -- empty hbox
	iup.Detach(hbox_list[1]) -- Logoff and close
	iup.Append(options_vbox, iup.hbox{mdi.ui.OptionsButton, iup.label{title = "MDI Options",},gap = 2,alignment = "ACENTER",})
	iup.Append(options_vbox, hbox_list[2])
	iup.Append(options_vbox, hbox_list[1])
	iup.Refresh(OptionsDialog)
end

--[[ *****************************
     * MDI Options Dialog Dialog *
     ***************************** ]]
local function CreateMDIOptionsDialog()

	local dlg, container
	local headerlabel
	local optionslistctl, optionslistsubdlg
	local okbutton, applybutton, closebutton
	local autohide_titlebar, autohide_border, focus_chat, show_canvas, resistance
	
	-- ### Listcontrol Content Section ###

	function change_state()
		applybutton.active = "YES"
	end
	
	local function makesubdlg(content)
		return iup.dialog{
			iup.vbox{
				content,
				iup.fill{size = 10},
			},
			border="NO",menubox="NO",resize="NO",
			fullscreen="NO",
			bgcolor = "0 0 0 0 *",
		}
	end

	local function clearlist()
		if optionslistctl[1] then
			optionslistctl[1] = nil
			for i in ipairs(optionslistctl.Items) do
				optionslistctl.Items[i]:destroy()
				optionslistctl.Items[i] = nil
			end
		end
	end

	local function setuplist()
		clearlist()

		-- Global Options
		autohide_titlebar = iup.stationtoggle{title = "Auto-hide Titlebar", value = mdi.autohide_titlebar and "ON" or "OFF", action = change_state}
		autohide_border = iup.stationtoggle{title = "Auto-hide Border", value = mdi.autohide_border and "ON" or "OFF", action = change_state}
		focus_chat = iup.stationtoggle{title = "Focus Chat", value = mdi.focus_chat and "ON" or "OFF", action = change_state}
		show_canvas = iup.stationtoggle{title = "Show Canvas (reload required)", value = mdi.show_canvas and "ON" or "OFF", action = change_state}
		resistance = iup.text{value = mdi.resistance, size = 25, action = change_state}

		optionslistctl.Items[1] = makesubdlg(iup.vbox{
			iup.label{title = "Global Options"},
			autohide_titlebar,
			autohide_border,
			focus_chat,
			show_canvas,
			iup.hbox{resistance, iup.label{title = "Edge Resistance"}, gap = 2},
		})
		iup.Append(optionslistctl, optionslistctl.Items[1])

		-- Window List
		local windows_vbox = iup.vbox{iup.label{title = "Windows"},}
		local sorted_window_list = {}
		for id in pairs(mdi.windows) do
			table.insert(sorted_window_list, id)
		end
		table.sort(sorted_window_list)
		
		for _, id in ipairs(sorted_window_list) do
			test.varprint(' / ', 'id', id, 'visible', mdi.windows[id].mdi_visible, 'value', mdi.windows[id].mdi_visible == "YES" and "ON" or "OFF")
			local title = (mdi.registered_content[id][2] and mdi.registered_content[id][2].title) or id
			local toggle = iup.stationtoggle{title = title, value = mdi.windows[id].mdi_visible == "YES" and "ON" or "OFF", action = change_state}
			mdi.ui.windows[id] = toggle
			iup.Append(windows_vbox, toggle)
		end
		
		optionslistctl.Items[2] = makesubdlg(windows_vbox)
		iup.Append(optionslistctl, optionslistctl.Items[2])

		optionslistctl:map()
		optionslistctl[1] = 1
	end

	-- ### Manager Dialog Section ###
	-- === Layout: Manager Dialog ================================================
	optionslistctl = iup.stationsublist{expand = "YES", control = "YES", border = "NO", Items = {}}

	headerlabel = iup.label{title = "MDI Options", font = Font.H2, expand = "HORIZONTAL"}
	okbutton = iup.stationbutton{title = "OK"}
	applybutton = iup.stationbutton{title = "Apply"}
	closebutton = iup.stationbutton{title = "Close"}
	
	container = iup.frame{
		iup.vbox{
			iup.hbox{headerlabel, iup.fill{}, optionsbutton, alignment = "ACENTER"},
			iup.hbox{optionslistctl,},
			iup.hbox{iup.fill{}, okbutton, applybutton, closebutton, gap = 10},
			gap = 5,
			margin = "7x7",
		},
		expand = "VERTICAL",
		bgcolor = "0 0 0 0 *",
		size = (xres >= 800 and 800 or xres).."x"
	}

	dlg = iup.dialog{
		iup.stationmainframebg{
			iup.hbox{
				iup.fill{},
				container,
				iup.fill{},
			},
		},
		bgcolor = "0 0 0",
		border="NO",menubox="YES",resize="NO",
		fullscreen="YES",
		defaultesc=closebutton,
	}
	dlg:map()

	-- === Callbacks: Manager Dialog ================================================
	function closebutton:action()
		HideDialog(iup.GetDialog(self))
		ShowDialog(OptionsDialog)
	end

	function applybutton:action()
		mdi.autohide_titlebar = autohide_titlebar.value == "ON"
		mdi.autohide_border = autohide_border.value == "ON"
		mdi.focus_chat = focus_chat.value == "ON"
		mdi.show_canvas = show_canvas.value == "ON"
		mdi.resistance = tonumber(resistance.value)
		
		gkini.WriteInt("MDI", "autohide_titlebar", mdi.autohide_titlebar and 1 or 0)
		gkini.WriteInt("MDI", "autohide_border", mdi.autohide_border and 1 or 0)
		gkini.WriteInt("MDI", "focus_chat", mdi.focus_chat and 1 or 0)
		gkini.WriteInt("MDI", "debug_show_canvas", mdi.show_canvas and 1 or 0)
		gkini.WriteInt("MDI", "resistance", mdi.resistance)
		
		for id, toggle in pairs(mdi.ui.windows) do
			local state = toggle.value == "ON" and "YES" or "NO"
			local pdlg = mdi.windows[id]
			if (pdlg.mdi_visible ~= state) then
				pdlg.mdi_visible = state
				pdlg.visible = state
				pdlg:save_position()
			end
		end

		self.active = "NO"
	end

	function okbutton:action()
		if (applybutton.active == "YES") then applybutton:action() end
		closebutton:action()
	end
	
	function dlg:show_cb()
		applybutton.active = "NO"
		setuplist()
	end

	dlg.optionslistctl = optionslistctl
	return dlg
end

mdi.ui.MDIOptionsDialog = CreateMDIOptionsDialog()
CreateOptionsButton()
