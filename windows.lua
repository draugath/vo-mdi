local set_scan_and_chat_default_positions, set_addons_group_default_positions
local default_positions = {}

default_positions['scaninfo'] = {x = math.floor(gkinterface.GetXResolution() * 0.75), y = HUD.scaninfo.y}
default_positions['secondarychatarea'] = {x = math.floor(gkinterface.GetXResolution() * 0.09), y = default_positions['scaninfo'].y}

default_positions['addons'] = {x = HUD.addonframe.x, y = HUD.addonframe.y}
default_positions['cargo'] = {x = default_positions['addons'].x, y = HUD.cargoframe.y}
default_positions['missionindicator'] = {x = default_positions['addons'].x, y = HUD.missionupdateindicator.y}
default_positions['BSinfo'] = {x = default_positions['addons'].x, y = HUD.BSinfo.friendlylabel.y}

test.print(test.stringify(default_positions, nil, true))
mdi:CreateWindow(
	"scaninfo",
	function(id)
		if (not mdi.registered_content[id][2].cx) then set_scan_and_chat_default_positions() end 
		iup.Detach(HUD.scaninfo)
		HUD.scaninfo.alignment = "ALEFT"
		return HUD.scaninfo
	end,
	{
		title = "Scan Info", 
		size = (gkinterface.GetXResolution() * 0.25).."x"..(gkinterface.GetYResolution() * 0.15), 
		cx = default_positions['scaninfo'].x, 
		cy = default_positions['scaninfo'].y, 
		border = "YES"
	}
)

mdi:CreateWindow(
	"secondarychatarea",
	function(id)
		iup.Detach(HUD.secondarychatarea)
		return HUD.secondarychatarea
	end,
	{
		title = "Secondary Chat", 
		size = (gkinterface.GetXResolution() / 3).."x"..(gkinterface.GetYResolution() * 0.15), 
		cx = default_positions['secondarychatarea'].x, 
		cy = default_positions['secondarychatarea'].y, 
		border = "NO"
	}
)

mdi:CreateWindow(
	"selfinfo",
	function(id)
		local selfinfo = iup.GetParent(HUD.selfinfoframe)
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = HUD.groupinfoframe.x, HUD.groupinfoframe.y
		iup.Detach(selfinfo)
		return selfinfo
	end,
	{title = "Self Info"}
)

mdi:CreateWindow(
	"addons",
	function(id)
		local addonframe = HUD.addonframe
		iup.Detach(addonframe)
		return addonframe
	end,
	{
		title = "Addons", 
		titlebar = "NO",
		cx = default_positions['addons'].x,
		cy = default_positions['addons'].y,
	}
)

mdi:CreateWindow(
	"cargo",
	function(id)
		local cargoframe = iup.GetParent(HUD.cargoframe)
		iup.Detach(cargoframe)
		return cargoframe
	end,
	{
		title = "Cargo", 
		titlebar = "NO",
		cx = default_positions['cargo'].x,
		cy = default_positions['cargo'].y,
	}
)

mdi:CreateWindow(
	"missionindicator",
	function(id)
		local mission = HUD.missionupdateindicator
		iup.Detach(mission)
		return mission
	end,
	{
		title = "Mission Update Indicator", 
		titlebar = "YES",
		cx = default_positions['missionindicator'].x,
		cy = default_positions['missionindicator'].y,
	}
)

mdi:CreateWindow(
	"BSinfo",
	function(id)
		local BSinfo = iup.GetParent(HUD.BSinfo.friendlylabel)
		iup.Detach(BSinfo)
		return BSinfo
	end,
	{
		title = "Border Skirmish Info", 
		titlebar = "NO",
		cx = default_positions['BSinfo'].x,
		cy = default_positions['BSinfo'].y,
	}
)

mdi:CreateWindow(
	"licensewatch",
	function(id)
		local licensewatch = iup.GetParent(HUD.licensewatchframe)
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = HUD.licensewatchframe.x, HUD.licensewatchframe.y
		iup.Detach(licensewatch)
		return licensewatch
	end,
	{title = "License Watch"}
)
	
mdi:CreateWindow(
	"targetframe",
	function(id)
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = HUD.targetframe.x, HUD.targetframe.y
		iup.Detach(HUD.targetframe)
		return HUD.targetframe
	end,
	{title = "Target Info"}
)

mdi:CreateWindow(
	"speedbar",
	function(id)
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = HUD.speedbar[1].x, HUD.speedbar[1].y
		iup.Detach(HUD.speedbar)
		return HUD.speedbar
	end,
	{title = "Speed Bar", titlebar = "NO", border = "NO"}
)

mdi:CreateWindow(
	"energybar",
	function(id)
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = HUD.energybar[1].x, HUD.energybar[1].y
		iup.Detach(HUD.energybar)
		return HUD.energybar
	end,
	{title = "Energy Bar", titlebar = "NO", border = "NO"}
)

mdi:CreateWindow(
	"distancebar",
	function(id)
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = HUD.distancebar[4][1].x, HUD.distancebar[4][1].y
		iup.Detach(HUD.distancebar)
		HUD.distancebar[3].size = nil
		HUD.distancebar[6].size = nil
		HUD.distancebar[8].size = nil
		return HUD.distancebar
	end,
	{title = "Distance Bar", titlebar = "NO", border = "NO"}
)

mdi:CreateWindow(
	"frontradar",
	function(id)
		local frontradar = HUD.radarlayer[2][1]
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = frontradar[2].x, frontradar[2].y
		iup.Detach(frontradar)
		return frontradar
	end,
	{title = "Front Radar", titlebar = "NO", border = "NO"}
)

mdi:CreateWindow(
	"backradar",
	function(id)
		local backradar = HUD.radarlayer[2][3]
		mdi.registered_content[id][2].cx, mdi.registered_content[id][2].cy = backradar[2].x, backradar[2].y
		iup.Detach(backradar)
		return backradar
	end,
	{title = "Back Radar", titlebar = "NO", border = "NO"}
)

mdi:CreateWindow(
	"chatwindow",
	function(id)
		local text_size = HUD.chatcontainer.chattext.size
		iup.Detach(HUD.chatframe)
		local function post(pdlg)
			HUD.chatcontainer.chattext.size = pdlg.stored_size or text_size
			pdlg.stored_size = nil
			pdlg[1][2][2][1].size = "x80" -- TODO change to calculate 3 lines of text based on current font-size default and font scaling
		end
		return HUD.chatframe, post
	end,
	{title = "Chat Window", resize = "YES", cx = 0, cy = 0}
)

-- RegisterEvent(function()
-- 	mdi:CreateWindow(targetlist.id, targetlist.display, {title = "Target List", cx = 350, cy = 350}, contextmenu)
-- 	targetlist.display[1][2][1].visible = "NO"
-- 	target_changed()
-- end, "PLUGINS_LOADED")

