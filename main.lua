declare("mdi", {
	autohide_border = gkini.ReadInt("MDI", "autohide_border", 0) == 1,
	autohide_titlebar = gkini.ReadInt("MDI", "autohide_titlebar", 1) == 1,
	focus_chat = gkini.ReadInt("MDI", "focus_chat", 0) == 1,
	resistance = gkini.ReadInt("MDI", "resistance", 50),
	show_canvas = gkini.ReadInt("MDI", "debug_show_canvas", 0) == 1,

	interact = false, -- DO NOT CHANGE; TOGGLED BY CODE BELOW
	registered_content = {},
	windows = {},
	edges = {
		-- Game-Window edges are flip-flopped to provide an edge that can be hit
		top = {
			[gkinterface.GetYResolution()] = {
				GAMEWINDOW = {
					0,
					gkinterface.GetXResolution(),
				},
			},
		},
		bottom = {
			[0] = {
				GAMEWINDOW = {
					0,
					gkinterface.GetXResolution(),
				},
			},
		},
		left = {
			[gkinterface.GetXResolution()] = {
				GAMEWINDOW = {
					0,
					gkinterface.GetYResolution(),
				},
			},
		},
		right = {
			[0] = {
				GAMEWINDOW = {
					0,
					gkinterface.GetYResolution(),
				},
			},
		},
	},
})
local old = {} -- for holding references to original functions before hooking them

local default_context_menu_structure = {
	{title = "SEPARATOR"},
	{
		title = "Window...",
		action = {
			{
				title = "Lock",
				type = "toggle",
				value = function(pdlg) return pdlg.locked == "YES" and "ON" or "OFF" end,
				action = function(pdlg) return pdlg.lock_window() end,
			},
			{
				title = "Shade",
				type = "toggle",
				value = function(pdlg) return pdlg.shaded == "YES" and "ON" or "OFF" end,
				titlebar_only = true,
				action = function(pdlg) return pdlg.shade_window() end,
			},
			{
				title = "Auto-Hide",
				type = "toggle",
				value = function(pdlg) return pdlg.autohide_window == "YES" and "ON" or "OFF" end,
				action = function(pdlg)
					local state = pdlg.autohide_window == "YES"
					pdlg.autohide_window = not state and "YES" or "NO"
					pdlg:save_position()
				end,
			},
			{
				title = "Hide",
				action = function(pdlg)
					pdlg.mdi_visible = "NO"
					pdlg.visible = "NO"
					pdlg:save_position()
				end,
			},
		},
	},
}

local direction_pairings = {
	top = 'bottom',
	bottom = 'top',
	left = 'right',
	right = 'left',
}

local IMAGES_DIR = "plugins/vo-mdi/"
local timer = {
	main = Timer(),
	cm = Timer(),
}

-- ### ################### ###
-- ### Debugging Functions ###
-- ### ################### ###
local function readargs(dlg, callback, ...)
	local str = ""
	for n = 1, select("#",...) do
		local e = select(n,...)
		local t = type(e)
		str = str.."\n\t"..n..": ("..t..")\t"..test.stringify(e)
	end
	console_print((iup.GetType(dlg).."("..tostring(dlg)..") - "..callback.." Data:"..str))
end

function mdi.CreateControl(func, params)
	assert(type(func) == "function", "Need a function in argument #1, dipshit")
	params = params or {}
	local ctrl

	ctrl = func(params)
	--do return ctrl end

	ctype = iup.GetType(ctrl)
	if (ctype ~= "canvas" and ctype ~= "cbox") then
		function ctrl:action(...) readargs(      self, "action", ...) end
	end
	function ctrl:button_cb(...) readargs(self, "button_cb", ...) end
	function ctrl:caret_cb(...) readargs(self, "caret_cb", ...) end
	function ctrl:close_cb(...) readargs(self, "close_cb", ...) end
	function ctrl:default_action(...) readargs(self, "default_action", ...) end
	function ctrl:draw_cb(...) readargs(self, "draw_cb", ...) end
	function ctrl:dropfiles_cb(...) readargs(self, "dropfiles_cb", ...) end
	function ctrl:enterwindow_cb(...) readargs(self, "enterwindow_cb", ...) end
	function ctrl:getfocus_cb(...) readargs(self, "getfocus_cb", ...) end
	function ctrl:height_cb(...) readargs(self, "height_cb", ...) end
	function ctrl:help_cb(...) readargs(self, "help_cb", ...) end
	function ctrl:highlight_cb(...) readargs(self, "highlight_cb", ...) end
	function ctrl:hspan_cb(...) readargs(self, "hspan_cb", ...) end
	function ctrl:idle_action(...) readargs(self, "idle_action", ...) end
	function ctrl:k_any(...) readargs(self, "k_any", ...) end
	if (ctype ~= "cbox") then
		function ctrl:keypress_cb(...) readargs(self, "keypress_cb", ...) end
	end
	--function ctrl:killfocus_cb(...) readargs(self, "killfocus_cb", ...) end
	function ctrl:leavewindow_cb(...) readargs(self, "leavewindow_cb", ...) end
	function ctrl:map_cb(...) readargs(self, "map_cb", ...) end
	function ctrl:mdiactivate_cb(...) readargs(self, "mdiactivate_cb", ...) end
	function ctrl:menuclose_cb(...) readargs(self, "menuclose_cb", ...) end
	function ctrl:mouseclick_cb(...) readargs(self, "mouseclick_cb", ...) end
	function ctrl:mousemotion_cb(...) readargs(self, "mousemotion_cb", ...) end
	if (ctype ~= "cbox" and ctype ~= "canvas") then
		function ctrl:motion_cb(...) readargs(self, "motion_cb", ...) end
	end
	function ctrl:ncols_cb(...) readargs(self, "ncols_cb", ...) end
	function ctrl:nlines_cb(...) readargs(self, "nlines_cb", ...) end
	function ctrl:open_cb(...) readargs(self, "open_cb", ...) end
	function ctrl:resize_cb(...) readargs(self, "resize_cb", ...) end
	function ctrl:scroll_cb(...) readargs(self, "scroll_cb", ...) end
	function ctrl:scrolling_cb(...) readargs(self, "scrolling_cb", ...) end
	function ctrl:show_cb(...) readargs(self, "show_cb", ...) end
	function ctrl:trayclick_cb(...) readargs(self, "trayclick_cb", ...) end
	function ctrl:vspan_cb(...) readargs(self, "vspan_cb", ...) end
	function ctrl:wheel_cb(...) readargs(self, "wheel_cb", ...) end
	function ctrl:width_cb(...) readargs(self, "width_cb", ...) end
	function ctrl:wom_cb(...) readargs(self, "wom_cb", ...) end

	for k, v in pairs(params) do
		if type(v) == "function" then 
			local old = ctrl[k]
			ctrl[k] = function(...) if (old) then old(...) end v(...) end
		end
	end

	return ctrl
end

--- ### ##################### ###
--- ### MDI Support Functions ###
--- ### ##################### ###

function strsplit(pattern,text,num)
	num = num or -1
	local count = 1
	local result = {}
    local theStart = 1
    local theSplitStart, theSplitEnd = string.find(text,pattern,theStart)
    while theSplitStart do
		if (num > 0 and count == num) then break end
		table.insert(result,string.sub(text,theStart,theSplitStart-1))
        theStart = theSplitEnd + 1
        theSplitStart, theSplitEnd = string.find(text,pattern,theStart)
		count = count + 1
    end
    table.insert(result, string.sub(text,theStart))
    return result 
end

local function between(i, j, k)
	if (i <= k and k <= j) then return true end
	return false
end

local function find_other_edge(travel, start, range, id)
	-- Find the edge of another window that is in the way of travel
	local opposing = direction_pairings[travel]
	local step = range < 0 and -1 or 1
	local edges, has_border, cur_edge, other_edge = mdi.edges, true
	
	for pos = start + step, start + range, step do
		other_edge = edges[opposing][pos]
		if (other_edge) then
			local cur_edge = edges[travel][start][id]
			for oe_id, dim in pairs(other_edge) do
				if (between(dim[1], dim[2], cur_edge[1]) or between(dim[1], dim[2], cur_edge[2]) or (cur_edge[1] < dim[1] and dim[2] < cur_edge[2])) then
					if (mdi.windows[oe_id]) then has_border = mdi.windows[oe_id].showborder ~= "NO" end
					return pos, has_border
				end
			end
		end
	end
end

function mdi:AutoHide()
	test.varprint(' / ', 'mdi:AutoHide() :: interact', self.interact, 'dont_hide', self.dont_hide)
	for id, pdlg in pairs(self.windows) do
		if (self.interact) then
			if (pdlg.autohide_window == "YES" and pdlg.mdi_visible ~= "NO") then
				pdlg.visible = "YES"
			end
			
			if (pdlg.titlebar ~= "NO" and self.autohide_titlebar) then
				pdlg[1][2][1].visible = "YES"
			end

			if (pdlg.showborder ~= "NO" and self.autohide_border) then
				pdlg[1][2][2].bgcolor = pdlg[1][2][2].bgcolor:gsub("(%d+ %d+ %d+) %d+ (%p)", "%1 255 %2")
			end

		else
			if (pdlg.autohide_window == "YES") then
				pdlg.visible = "NO"
			end
			
			if (pdlg.titlebar ~= "NO" and self.autohide_titlebar) then
				pdlg[1][2][1].visible = "NO"
			end

			if (pdlg.showborder ~= "NO" and self.autohide_border) then
				pdlg[1][2][2].bgcolor = pdlg[1][2][2].bgcolor:gsub("(%d+ %d+ %d+) %d+ (%p)", "%1 0 %2")
			end
		end
	end
end

local function create_window(id)
	if (not mdi.registered_content[id]) then return end

	local content, post = mdi.registered_content[id][1]
	if (type(content) == "function") then content, post = content(id) end
	if (type(post) ~= "function") then post = nil end
	local pdlg = mdi.pseudo_dialog(content, id, mdi.registered_content[id][2])
	mdi.windows[id] = pdlg
	return pdlg, post
end

function mdi:CreateWindow(id, content, params, contextmenu)
	assert(type(id) == "string", "Need a string in argument #1, dipshit")
	assert(type(content) == "userdata" or type(content) == "function", "Need a userdata or function in argument #2, dipshit")
	if (params) then assert(type(params) == "table", "Need a table in argument #3, dipshit") end
	if (contextmenu) then assert(type(contextmenu) == "table", "Need a table in argument #4, dipshit") end

	params = params or {}
	if (self.windows[id]) then return end
	self.registered_content[id] = {content, params, contextmenu}
	if (type(self.cbox) == "userdata") then 
		local pdlg, post = create_window(id)
		iup.Append(iup.GetNextChild(self.cbox), pdlg) -- Needs to be inserted into the hbox inside of the cbox
		self.cbox:map()
		if (post) then post(pdlg) end
		pdlg:set_window_bounds()
	end
end

function mdi:DestroyWindow(id)
	assert(type(id) == "string", "Need a string in argument #1, dipshit")
	iup.Detach(self.windows[id])
	iup.Destroy(self.windows[id])
	self.windows[id] = nil
	self.registered_content[id] = nil
end

function mdi:GetWindow(id)
	assert(type(id) == "string", "Need a string in argument #1, dipshit")
	return self.windows[id]
end

local function get_context_menu_item_parent(ctl)
	return iup.GetParent(iup.GetParent(iup.GetParent(iup.GetParent(iup.GetParent(iup.GetParent(ctl))))))
end

function mdi.contextmenu(pdlg, structure)
	structure = structure or {}

	local items = {gap = 2}
	local menu_group = {}
	local index = 1
	while (index <= #structure) do
		local item = structure[index]
		if (not item.titlebar_only or (item.titlebar_only and pdlg.titlebar ~= "NO")) then
	
			local title

			if (type(item.title) == "function") then title = item.title(pdlg) else title = tostring(item.title) end

			if (title:upper() ~= "SEPARATOR") then
				-- using the action callback on an actual toggle caused seg faults, so we're faking it.
				local item_bg = iup.label{title = "", bgcolor = ListColors[0], expand = "HORIZONTAL"}
				local item_value = type(item.value) == "function" and item.value(pdlg) or item.value
				local toggle = iup.stationtoggle{visible = item.type == "toggle" and "YES" or "NO", value = item_value}
				local label = mdi.CreateControl(iup.label,{title = title, bgcolor = "0 0 0 0 *", expand = "HORIZONTAL", border = "YES"})
				local action = mdi.CreateControl(iup.canvas, {bgcolor = "0 255 0", border = mdi.show_canvas and "YES" or "NO"})

				function action:enterwindow_cb()
					if (timer.cm:IsActive()) then timer.cm:Kill() end

					item_bg.bgcolor = ListColors[2]
					local parent = get_context_menu_item_parent(self)

					if ( parent.submenu and (not self.submenu or (self.submenu and parent.submenu ~= self.submenu)) ) then
						return mdi.destroycontextmenu(parent.submenu)
					end
					
					if (type(item.action) == "table" and not parent.submenu) then
						local menu = mdi.contextmenu(pdlg, item.action)
						menu.cx, menu.cy = item_bg.x + item_bg.size:match("(%d+)x") + 4, item_bg.y - 4
						iup.Append(iup.GetParent(pdlg), menu)
						mdi.cbox:map()
						mdi.cbox.REFRESH = "YES"

						-- assigning submenu to both the parent and the item for comparison
						self.submenu = menu
						parent.submenu = menu
						menu.parentmenu = parent
					end
				end

				function action:leavewindow_cb() 
					item_bg.bgcolor = ListColors[0] 
					timer.cm:SetTimeout(250, mdi.destroycontextmenu)
				end

				function action:button_cb(b, s, x, y, f) 
					if (s == 0 and type(item.action) == "function") then
						item.action(pdlg)
						timer.cm:SetTimeout(1, mdi.destroycontextmenu)
					end
				end

				-- action canvas has been widened to cover the edges of the frame to prevent losing the context menu when moving between them
				table.insert(menu_group, iup.zbox{item_bg, iup.hbox{toggle, label}, iup.hbox{action, margin = "-4x"}, all = "YES"})

			else
				if (#menu_group > 0) then 
					table.insert(items, iup.vbox(menu_group)) 
					menu_group = {}
				end
			end
		end
		index = index + 1
	end

	-- Used to prevent the separator from killing context menus
	local menubacklayer = iup.canvas{expand = "YES", border = "NO",}
	function menubacklayer:enterwindow_cb()
		if (timer.cm:IsActive()) then timer.cm:Kill() end
		local parent = iup.GetParent(iup.GetParent(iup.GetParent(self)))
		if (parent.submenu) then
			return mdi.destroycontextmenu(parent.submenu)
		end
	end
	function menubacklayer:leavewindow_cb()
		timer.cm:SetTimeout(250, mdi.destroycontextmenu)
	end

	table.insert(items, iup.vbox(menu_group)) 
	local cm = mdi.CreateControl(iup.pdasubsubsubframefull2, {
		iup.zbox{
			menubacklayer,
			iup.vbox(items),
			all = "YES",
		},
		size = "100x",
	})
	return cm
end

function mdi.destroycontextmenu(menu, c)
	-- c originally added for debugging, but also works to detect the first menu being destroyed
	c = c or 1
	menu = menu or mdi.context_menu
	
	local sub
	if (type(menu) == "userdata") then
		if (c == 1) then
			if (menu.parentmenu) then menu.parentmenu.submenu = nil
			else mdi.context_menu = nil
			end
		end

		if (menu.submenu) then sub = menu.submenu end
		
		iup.Detach(menu)
		iup.Destroy(menu)
	end

	if (sub) then 	
		c = c + 1
		return mdi.destroycontextmenu(sub, c)
	end
end

-- ### ######################## ###
-- ### Pseudo-Dialog Definition ###
-- ### ######################## ###

function mdi.pseudo_dialog(content, id, params)
	assert(type(content) == "userdata", "Need a userdata in argument #1, dipshit")
	assert(type(id) == "string", "Need a string in argument #2, dipshit")
	if (params) then assert(type(params) == "table", "Need a table in argument #3, dipshit") end

	params = params or {}

	local pdlg, main, title_bar, contents
	local move_canvas
	local resize_layer, resize_top, resize_bottom, resize_left, resize_right
	local resize_nw, resize_ne, resize_sw, resize_se

	local default_margin = 0 -- ???
	local resize_bar_width = params.resize ~= "YES" and 0 or 7
	local half_resize_width = math.floor(resize_bar_width/2)
	local adjusted_margin = -default_margin - half_resize_width

	local stored_config = gkini.ReadString("MDI", id, "")
	local stored_vals = strsplit(",", stored_config)
	for i, v in pairs(stored_vals) do if (v == "") then stored_vals[i] = nil end end
	--test.print(test.stringify(stored_vals))

	move_canvas = mdi.CreateControl(iup.canvas,{bgcolor = "255 0 0", border = mdi.show_canvas and "YES" or "NO"})
--	move_canvas = iup.canvas{bgcolor = "255 0 0", border = mdi.show_canvas and "YES" or "NO"}
	resize_top = iup.canvas{size = "x"..resize_bar_width, expand = "HORIZONTAL", bgcolor = "101 102 255", border = mdi.show_canvas and "YES" or "NO"}
	resize_bottom = iup.canvas{size = "x"..resize_bar_width, expand = "HORIZONTAL", bgcolor = "101 102 255", border = mdi.show_canvas and "YES" or "NO"}
	resize_left = iup.canvas{size = resize_bar_width.."x", expand = "VERTICAL", bgcolor = "101 102 255", border = mdi.show_canvas and "YES" or "NO"}
	resize_right = iup.canvas{size = resize_bar_width.."x", expand = "VERTICAL", bgcolor = "101 102 255", border = mdi.show_canvas and "YES" or "NO"}
	resize_nw = iup.canvas{size = resize_bar_width.."x"..resize_bar_width, expand = "NO", bgcolor = "0 255 255", border = mdi.show_canvas and "YES" or "NO"}
	resize_ne = iup.canvas{size = resize_bar_width.."x"..resize_bar_width, expand = "NO", bgcolor = "0 255 255", border = mdi.show_canvas and "YES" or "NO"}
	resize_sw = iup.canvas{size = resize_bar_width.."x"..resize_bar_width, expand = "NO", bgcolor = "0 255 255", border = mdi.show_canvas and "YES" or "NO"}
	resize_se = iup.canvas{size = resize_bar_width.."x"..resize_bar_width, expand = "NO", bgcolor = "0 255 255", border = mdi.show_canvas and "YES" or "NO"}

	resize_layer = iup.vbox{
		iup.hbox{
			resize_nw, resize_top, resize_ne
		},
		iup.hbox{
			resize_left, iup.fill{}, resize_right,
		},
		iup.hbox{
			resize_sw, resize_bottom, resize_se
		},
		margin = adjusted_margin.."x"..adjusted_margin,
		visible = params.resize ~= "YES" and "NO" or "YES"
	}

	contents = mdi.CreateControl(iup.frame, {
			content,
			image = IMAGES_DIR..'border.png',
			segmented="0.28125 0.28125 0.65625 0.65625",
			bgcolor = "255 255 255 "..(params.border ~= "NO" and 255 or 0).." *",
			size = params.size,
			visible = stored_vals[6] == "YES" and "NO" or "YES",
	})

	if (params.titlebar ~= "NO") then
		title_bar = iup.zbox{
			iup.pdarootframe{
				iup.hbox{
					iup.label{title = params.title or "", expand = "YES"},
				},
			},
			iup.vbox{
				iup.fill{size = half_resize_width},
				iup.hbox{ 
					iup.fill{size = half_resize_width}, 
					move_canvas,
					iup.fill{size = half_resize_width}, 
				},
			},
			all = "YES"
		}

		main = iup.vbox{
			title_bar,
			contents,
		}

	else
		main = iup.zbox{
			move_canvas,
			contents,
			all = "YES",
		}
	end

	pdlg = iup.pdarootframebg{
		iup.zbox{
			resize_layer,
			main,
			all = "YES",
		},
		bgcolor = "255 255 255 0 *",
		expand = "NO",
		contents = contents,

		autohide_window = stored_vals[7],
		cx = math.floor(stored_vals[1] or params.cx),
		cy = math.floor(stored_vals[2] or params.cy),
		locked = stored_vals[5],
		mdi_visible = stored_vals[4] or "YES", -- Needed for evaluating the visible state of a pdialog in the options dialog
		shaded = stored_vals[6],
		showborder = params.border ~= "NO" and "YES" or "NO",
		stored_size = stored_vals[3],
		titlebar = params.titlebar,
		visible = stored_vals[4] or "YES",
	}

	-- ### Callbacks ###
	local doubleclick, dragging, startx, starty
	local force_x, force_y = 0, 0 -- How much force has been applied along a certain axis to defeat window reistance

	function pdlg:fix_contents_width()
		if (self.titlebar ~= "NO") then
			local pd_width = tonumber(self.size:match("(%d+)x"))
			local co_width = tonumber(contents.size:match("(%d+)x"))
			if (co_width < pd_width) then 
				contents.size = pd_width.."x"
				mdi.cbox.REFRESH = "YES"
			end
		end
	end

	function pdlg:save_position()
		local pos = self.cx..","..self.cy                           -- 1, 2
		local size = ","                                            -- 3
		local visible = ","..(self.mdi_visible or "YES")            -- 4
		local locked = ","..(self.locked or "NO")                   -- 5
		local shaded = ","..(self.shaded or "NO")                   -- 6
		local autohide_window = ","..(self.autohide_window or "NO") -- 7

		if (params.resize == "YES" or params.resize == nil) then 
			if (id == "chatwindow") then
				size = size..HUD.chatcontainer.chattext.size
			else
				size = size..self.size
			end
		end
		--test.print('config', pos..size..visible..locked..shaded..autohide_window)
		gkini.WriteString("MDI", id, pos..size..visible..locked..shaded..autohide_window)
	end

	function pdlg:set_window_bounds()
		local width, height = self.size:match("(%d+)x(%d+)")
		self.width = tonumber(width) - 1
		self.height = tonumber(height) - 1
		test.varprint(' / ', 'id', id, 'cx', self.cx, 'cy', self.cy, 'w', self.width, 'h', self.height)
		local top, bottom = tonumber(self.cy), tonumber(self.cy) + self.height
		local left, right = tonumber(self.cx), tonumber(self.cx) + self.width
		local edges = mdi.edges

		edges.top[top] = edges.top[top] or {}
		edges.top[top][id] = {left, right}

		edges.bottom[bottom] = edges.bottom[bottom] or {}
		edges.bottom[bottom][id] = {left, right}

		edges.left[left] = edges.left[left] or {}
		edges.left[left][id] = {top, bottom}

		edges.right[right] = edges.right[right] or {}
		edges.right[right][id] = {top, bottom}
		
	end
	
	function pdlg:clear_window_bounds()
		local top, bottom = tonumber(self.cy), self.cy + self.height
		local left, right = tonumber(self.cx), self.cx + self.width
		local edges = mdi.edges

		edges.top[top][id] = nil
		if (not next(edges.top[top])) then edges.top[top] = nil end
		edges.bottom[bottom][id] = nil
		if (not next(edges.bottom[bottom])) then edges.bottom[bottom] = nil end
		edges.left[left][id] = nil
		if (not next(edges.left[left])) then edges.left[left] = nil end
		edges.right[right][id] = nil
		if (not next(edges.right[right])) then edges.right[right] = nil end
	end
	
	function pdlg.lock_window()
		local state = pdlg.locked == "YES"
		pdlg.locked = not state and "YES" or "NO"
		pdlg:save_position()
	end

	function pdlg.shade_window()
		doubleclick = true
		return move_canvas:button_cb(nil, 0)
	end

	old.button_cb = move_canvas.button_cb
	function move_canvas:button_cb(b, s, x, y, f)
		if (old.button_cb) then old.button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end

		if (s == 1) then -- press
			if (b == 8388616) then
				-- Left mouse double click
				if (pdlg.titlebar ~= "NO") then doubleclick = true end
			elseif (b == 32) then
				-- Right mouse click
			elseif (b == 8) then
				dragging = true
				startx = x
				starty = y
			end

		else -- release
			if (b == 32) then
				-- Release right mouse click
				local context_menu_structure = {}
				for i, v in ipairs(mdi.registered_content[id][3] or {}) do table.insert(context_menu_structure, v) end
				for i, v in ipairs(default_context_menu_structure) do table.insert(context_menu_structure, v) end
				local menu = mdi.contextmenu(pdlg, context_menu_structure)
				menu.cx, menu.cy = x - 15, y - 8 -- not sure why to subtract, but that's what gives the result I want
				iup.Append(iup.GetParent(pdlg), menu)
				mdi.cbox:map()
				mdi.cbox.REFRESH = "YES"
				mdi.context_menu = menu

			else
				mdi.dont_hide = true -- ### A hack needed to keep lists of controls from auto-hiding
				dragging = nil
				startx = nil
				starty = nil

				if (doubleclick) then
					doubleclick = nil
					pdlg.shaded = pdlg.shaded == "YES" and "NO" or "YES"
					local new_state = pdlg.shaded == "YES" and "NO" or "YES"
					contents.visible = new_state
					if (new_state == "YES") then
						if (type(content.show_cb) == "function") then content:show_cb() end
					else 
						if (type(content.hide_cb) == "function") then content:hide_cb() end
					end
					-- Refresh not needed after this.
					-- Unshading causes listboxes-of-controls to not reshow their contents.  Refresh and Map have no effect on this.
					-- Detach and Append has been proven to redisplay contents, but seems to only be necessary for listboxes.
					timer.main:SetTimeout(1, function() move_canvas:button_cb(nil, 0) end)
					test.print('end double_click')

				else
					-- Detach and re-Append to move the pdialog to the top of the z-order
					local cur_focus = iup.GetFocus()
					local cur_text = HUD.chatcontainer.chatentry.value
					local parent = iup.GetParent(pdlg)
					iup.Detach(pdlg)
					iup.Append(parent, pdlg)
					mdi.cbox:map()
					if (id == "chatwindow") then
						HUD.chatcontainer.chattext.scroll = "BOTTOM"
						if (cur_focus == HUD.chatcontainer.chatentry) then 
							-- Doing this causes an anomaly with mdi.cbox:getfocus_cb().  But it still appears to trigger after a jump.
							iup.SetFocus(cur_focus) 
							cur_focus.value = cur_text
						end
					end
					ProcessEvent("MDI_ZORDER_SHIFT", id) -- Trigger an event so listboxes can be refreshed properly
					test.print('end else')
					if (b == nil) then return end -- nil if called after a double-click release, see above.
				end

				mdi.dont_hide = nil
				test.varprint(' / ', 'after_append: dont_hide', mdi.dont_hide)
				-- Because some content can become visible after being Appended even if it's parent is VISIBLE = "NO"
				content.visible = pdlg.shaded == "YES" and "NO" or "YES"
				pdlg:save_position()
			end
		end
	end

	old.motion_cb = move_canvas.motion_cb
	function move_canvas:motion_cb(x, y, f)
		if (dragging and old.motion_cb) then old.motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end

		if (dragging) then
			local dx = x - tonumber(startx)
			local dy = y - tonumber(starty)
			startx = x
			starty = y

			if (f:sub(1,1) ~= "S") then
				local edge, other_edge, edge_has_border
		
				if (dx < 0) then
					-- Travelling Left
					if (force_x > 0) then force_x = 0 end
					edge = tonumber(pdlg.cx)
					other_edge, edge_has_border = find_other_edge('left', edge, dx, id)
					if (other_edge and (edge_has_border or f:sub(2,2) == "C") and edge + dx <= other_edge)  then 
						if (force_x == 0) then force_x = -1 else force_x = force_x + dx end
						if (math.abs(force_x) < mdi.resistance) then
							dx = other_edge - edge + 1
						else
							force_x = 0
						end
					end

				elseif (dx > 0) then
					-- Travelling Right
					if (force_x < 0) then force_x = 0 end
					edge = tonumber(pdlg.cx) + tonumber(pdlg.width)
					other_edge, edge_has_border = find_other_edge('right', edge, dx, id)
					if (other_edge and (edge_has_border or f:sub(2,2) == "C") and edge + dx >= other_edge) then 
						if (force_x == 0) then force_x = 1 else force_x = force_x + dx end
						if (math.abs(force_x) < mdi.resistance) then
							dx = other_edge - edge - 1
						else
							force_x = 0
						end
					end
				end

				if (dy < 0) then
					-- Travelling Up
					if (force_y > 0) then force_y = 0 end
					edge = tonumber(pdlg.cy)
					other_edge, edge_has_border = find_other_edge('top', edge, dy, id)
					if (other_edge and (edge_has_border or f:sub(2,2) == "C") and edge + dy <= other_edge) then 
						if (force_y == 0) then force_y = -1 else force_y = force_y + dy end
						if (math.abs(force_y) < mdi.resistance) then
							dy = other_edge - edge + 1 
						else
							force_y = 0
						end
					end

				elseif (dy > 0) then
					-- Travelling Down
					if (force_y < 0) then force_y = 0 end
					edge = tonumber(pdlg.cy) + tonumber(pdlg.height)
					other_edge, edge_has_border = find_other_edge('bottom', edge, dy, id)
					if (other_edge and (edge_has_border or f:sub(2,2) == "C") and edge + dy >= other_edge) then 
						if (force_y == 0) then force_y = 1 else force_y = force_y + dy end
						if (math.abs(force_y) < mdi.resistance) then
							dy = other_edge - edge - 1 
						else
							force_y = 0
						end
					end
				end
			end

			pdlg:clear_window_bounds()
			pdlg.cx = tonumber(pdlg.cx) + dx
			pdlg.cy = tonumber(pdlg.cy) + dy
			mdi.cbox.REFRESH = "YES"
			pdlg:set_window_bounds()
		end
	end

	old.resize_top_button_cb = resize_top.button_cb
	function resize_top:button_cb(b, s, x, y, f)
		if (old.resize_top_button_cb) then old.resize_top_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end

		if (s == 1) then -- press
			dragging = true
			--startx = x
			starty = y
		else -- release
			pdlg:save_position()
			dragging = nil
			--startx = nil
			starty = nil
			HUD.chatcontainer.chattext.scroll = "BOTTOM"
		end
	end

	old.resize_top_motion_cb = resize_top.motion_cb
	function resize_top:motion_cb(x, y, f)
		if (dragging and old.resize_top_motion_cb) then old.resize_top_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end

		if (dragging) then
			local dx --= x - tonumber(startx)
			local dy = y - tonumber(starty)
			--local width, height = contents.size:match("(%d+)x(%d+)")
			local width, height = HUD.chatcontainer.chattext.size:match("(%d+)x(%d+)")
			--width = width + dx
			height = height - dy
			--startx = x
			starty = y
			pdlg:clear_window_bounds()
			--contents.size = width.."x"..height
			contents.size = contents.size:match("%d+x")
			HUD.chatcontainer.chattext.size = width.."x"..height
			--HUD.chatcontainer.chattext.size = nil
			HUD.chatcontainer.chatentry.size = nil
			pdlg.size = nil
			pdlg.cy = pdlg.cy + dy
			iup.Refresh(pdlg)
			pdlg:set_window_bounds()
		end
	end
	
	old.resize_bottom_button_cb = resize_bottom.button_cb
	function resize_bottom:button_cb(b, s, x, y, f)
		if (old.resize_bottom_button_cb) then old.resize_bottom_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end

		if (s == 1) then -- press
			dragging = true
			--startx = x
			starty = y
		else -- release
			pdlg:save_position()
			dragging = nil
			--startx = nil
			starty = nil
			HUD.chatcontainer.chattext.scroll = "BOTTOM"
		end
	end

	old.resize_bottom_motion_cb = resize_bottom.motion_cb
	function resize_bottom:motion_cb(x, y, f)
		if (dragging and old.resize_bottom_motion_cb) then old.resize_bottom_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end

		if (dragging) then
			local dx --= x - tonumber(startx)
			local dy = y - tonumber(starty)
			--local width, height = contents.size:match("(%d+)x(%d+)")
			local width, height = HUD.chatcontainer.chattext.size:match("(%d+)x(%d+)")
			--width = width + dx
			height = height + dy
			--startx = x
			starty = y
			pdlg:clear_window_bounds()
			--contents.size = width.."x"..height
			contents.size = contents.size:match("%d+x")
			HUD.chatcontainer.chattext.size = width.."x"..height
			--HUD.chatcontainer.chattext.size = nil
			HUD.chatcontainer.chatentry.size = nil
			pdlg.size = nil
			iup.Refresh(pdlg)
			pdlg:set_window_bounds()
		end
	end

	old.resize_left_button_cb = resize_left.button_cb
	function resize_left:button_cb(b, s, x, y, f)
		if (old.resize_left_button_cb) then old.resize_left_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end

		if (s == 1) then -- press
			dragging = 0
			startx = x
			--starty = y
		else -- release
			pdlg:save_position()
			dragging = nil
			startx = nil
			--starty = nil
			HUD.chatcontainer.chattext.scroll = "BOTTOM"
		end
	end

	old.resize_left_motion_cb = resize_left.motion_cb
	function resize_left:motion_cb(x, y, f)
		if (dragging and old.resize_left_motion_cb) then old.resize_left_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end

		if (dragging) then
			local dx = x - tonumber(startx)
			local dy --= y - tonumber(starty)
			local width, height = contents.size:match("(%d+)x(%d+)")
			width = width - dx
			--height = height + dy
			startx = x
			--starty = y
			pdlg:clear_window_bounds()
			contents.size = width.."x"..height
			HUD.chatcontainer.chattext.size = HUD.chatcontainer.chattext.size:match("x%d+")
			HUD.chatcontainer.chatentry.size = nil
			pdlg.size = nil
			pdlg.cx = pdlg.cx + dx
			iup.Refresh(pdlg)
			pdlg:set_window_bounds()
		end
	end

	old.resize_right_button_cb = resize_right.button_cb
	function resize_right:button_cb(b, s, x, y, f)
		if (old.resize_right_button_cb) then old.resize_right_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end

		if (s == 1) then -- press
			dragging = 0
			startx = x
			--starty = y
		else -- release
			pdlg:save_position()
			dragging = nil
			startx = nil
			--starty = nil
			HUD.chatcontainer.chattext.scroll = "BOTTOM"
		end
	end

	old.resize_right_motion_cb = resize_right.motion_cb
	function resize_right:motion_cb(x, y, f)
		if (dragging and old.resize_right_motion_cb) then old.resize_right_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end

		if (dragging) then
			local dx = x - tonumber(startx)
			local dy --= y - tonumber(starty)
			local width, height = contents.size:match("(%d+)x(%d+)")
			width = width + dx
			--height = height + dy
			startx = x
			--starty = y
			pdlg:clear_window_bounds()
			contents.size = width.."x"..height
			HUD.chatcontainer.chattext.size = HUD.chatcontainer.chattext.size:match("x%d+")
			HUD.chatcontainer.chatentry.size = nil
			pdlg.size = nil
			iup.Refresh(pdlg)
			pdlg:set_window_bounds()
		end
	end

	old.resize_nw_button_cb = resize_nw.button_cb
	function resize_nw:button_cb(b, s, x, y, f)
		if (old.resize_nw_button_cb) then old.resize_nw_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end
	end

	old.resize_nw_motion_cb = resize_nw.motion_cb
	function resize_nw:motion_cb(x, y, f)
		if (dragging and old.resize_nw_motion_cb) then old.resize_nw_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end
	end

	old.resize_ne_button_cb = resize_ne.button_cb
	function resize_ne:button_cb(b, s, x, y, f)
		if (old.resize_ne_button_cb) then old.resize_ne_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end
	end

	old.resize_ne_motion_cb = resize_ne.motion_cb
	function resize_ne:motion_cb(x, y, f)
		if (dragging and old.resize_ne_motion_cb) then old.resize_ne_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end
	end

	old.resize_sw_button_cb = resize_sw.button_cb
	function resize_sw:button_cb(b, s, x, y, f)
		if (old.resize_sw_button_cb) then old.resize_sw_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end
	end

	old.resize_sw_motion_cb = resize_sw.motion_cb
	function resize_sw:motion_cb(x, y, f)
		if (dragging and old.resize_sw_motion_cb) then old.resize_sw_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end
	end

	old.resize_se_button_cb = resize_se.button_cb
	function resize_se:button_cb(b, s, x, y, f)
		if (old.resize_se_button_cb) then old.resize_se_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end
	end

	old.resize_se_motion_cb = resize_se.motion_cb
	function resize_se:motion_cb(x, y, f)
		if (dragging and old.resize_se_motion_cb) then old.resize_se_motion_cb(self, x, y, f) end
		if (pdlg.locked == "YES") then return end
	end

	return pdlg
end

-- ### ################## ###
-- ### MDI Initialization ###
-- ### ################## ###

function mdi:BuildMDI()
	local cbox_contents = {size = HUDSize(1,1)}
	-- Add previously registered windows if HUD:Reload() was called
	local post_processing = {}
	for id, data in pairs(self.registered_content) do
		local pdlg, post = create_window(id)
		post_processing[pdlg] = post
		table.insert(cbox_contents, pdlg)
	end

	self.cbox = self.CreateControl(iup.cbox, (cbox_contents))
	HUD.mdilayer = self.cbox
	table.insert(HUD.layers, self.cbox)
	iup.Append(HUD.dlg[1][1][2], self.cbox)

	self.cbox:map()

	-- Perform post-processing on pseudo-dialogs
	for pdlg, post in pairs(post_processing) do
		post(pdlg)
	end
	post_processing = nil
	
	-- Detect window bounds and initialize edge list and fix widths
	for id, pdlg in pairs(self.windows) do
		pdlg:fix_contents_width()
		pdlg:set_window_bounds()
	end

	-- ### CBox Callbacks ###
	old.cbox_button_cb = self.cbox.button_cb
	function mdi.cbox:button_cb(b, s, x, y, f)
		if (type(old.cbox_button_cb) == "function") then old.cbox_button_cb(self, b, s, x, y, f) end
		--if (mdi.context_menu) then mdi.destroycontextmenu() end
	end

	old.cbox_getfocus_cb = self.cbox.getfocus_cb
	function mdi.cbox:getfocus_cb()
		if (mdi.dont_hide) then return end
		if (old.cbox_getfocus_cb) then old.cbox_getfocus_cb(self) end
		gkinterface.HideMouse()
		Game.SetInputMode(1)
	end

	-- ### Function Hooks ###
	old.dlg_k_any = HUD.dlg.k_any
	function HUD.dlg:k_any(c)
		local cmd = gkinterface.GetCommandForKeyboardBind(c)
		if (cmd == "Quit") then 
			gkinterface.HideMouse()
			Game.SetInputMode(1)
		else
			gkinterface.GKProcessCommand(cmd)
		end
		if (type(old.dlg_k_any) == "function") then old.dlg_k_any(self, c) end
	end

	old.dlg_getfocus_cb = HUD.dlg.getfocus_cb
	function HUD.dlg:getfocus_cb(...)
		test.varprint(' / ', 'HUD.dlg.getfocus_cb() :: dont_hide', mdi.dont_hide)
		if (mdi.dont_hide) then return end
		if (type(old.dlg_getfocus_cb) == "function") then old.dlg_getfocus_cb(self, ...) end
	end

	old.HUD_setup_visible_elements = HUD.setup_visible_elements
	function HUD:setup_visible_elements()
		test.print("HUD:setup_visible_elements()")
		old.HUD_setup_visible_elements(self)
		for id, pdlg in pairs(mdi.windows) do
			local isvisible = pdlg.mdi_visible
			if (isvisible == "YES" and pdlg.autohide_window == "YES") then isvisible = "NO" end
			pdlg.visible = isvisible
		end
	end

	old.HUD_ShowEverything = HUD.ShowEverything
	function HUD:ShowEverything()
		old.HUD_ShowEverything(self)
		for id, pdlg in pairs(mdi.windows) do 
			pdlg.visible = "YES"
		end
	end

	old.HUD_HideMost = HUD.HideMost
	function HUD:HideMost()
		old.HUD_HideMost(self)
		self.mdilayer.visible = "YES"
		for id, pdlg in pairs(mdi.windows) do
			pdlg.visible = "NO"
		end
		mdi.windows["chatwindow"].visible = "YES"
	end

	old.HUD_Reload = HUD.Reload
	function HUD:Reload(...)
		test.varprint(' / ', 'HUD:Reload() :: interact', mdi.interact, 'dont_hide', mdi.dont_hide)
		for id, data in pairs(mdi.registered_content) do 
			if (type(data[1]) == "userdata") then 
				iup.Detach(data[1])
			end
		end
		Game.SetInputMode = old.Game_SetInputMode
		if (type(old.HUD_Reload) == "function") then old.HUD_Reload(self, ...) end
		mdi:BuildMDI()
		iup.Refresh(HUD.dlg)
		mdi:AutoHide()
		HUD.chatcontainer.chattext.scroll = "BOTTOM"
		ProcessEvent("MDI_rHUDxscale_FINISHED")
	end

	UnregisterEvent(HUD, "CHAT_DONE")
	function mdi:CHAT_DONE(event, ...)
		if (not self.focus_chat) then
			HUD:cancel_chat()
		end
	end
	RegisterEvent(mdi, "CHAT_DONE")

	old.Game_SetInputMode = Game.SetInputMode
	function Game.SetInputMode(v)
		test.varprint(' / ', 'Game.SetInputMode() :: interact' , mdi.interact, 'dont_hide', mdi.dont_hide)
		old.Game_SetInputMode(v)
		mdi.interact = false
		mdi.destroycontextmenu()
		mdi:AutoHide()
	end

end


--[[
 ##    ##   ##   #### ##   ##
 ###  ### ##  ##  ##  ###  ##
 ## ## ## ######  ##  ## # ##
 ##    ## ##  ##  ##  ##  ###
 ##    ## ##  ## #### ##   ##
]]

dofile("windows.lua")
dofile("options.lua")

mdi:BuildMDI()

old.HideTooltip = HideTooltip
function HideTooltip(...)
	test.print('HideTooltip()')
	if (mdi.interact) then mdi.dont_hide = true end
	if (type(old.HideTooltip) == "function") then old.HideTooltip(...) end
	if (mdi.interact) then mdi.dont_hide = nil end
end

old.gkinterface_HideMouse = gkinterface.HideMouse
function gkinterface.HideMouse(...)
	test.print('gkinterface.HideMouse()')
	if (type(old.gkinterface_HideMouse) == "function") then old.gkinterface_HideMouse(...) end
end

function mdi:COMMAND(_, id)
	test.print("Enter Chat")
	if (id <= 16 and id >= 13) then
		if (not self.interact) then 
			self.interact = true 
			mdi.destroycontextmenu()
			self:AutoHide()
		end
	end
end
RegisterEvent(mdi, "COMMAND")

--[[
TODO: test mdi:CreateWindow() with a userdata object instead of function

[12:13] <ARF> each of the power bars, 3000m distance thingy, target info, each radar, ship damage display, cargo icons, addon icons, group list, idk? 
[12:13] <ARF> chat area, secondarymessage area
[12:13] <ARF>  scan results
[12:14] <ARF> skirmish scoreboard


[Mon Nov  4 17:48:30 2013] *Phaserlight* well, the three commands are "Come" "Hold" and "Recall"



TODO: create a hook for 3rd-party coloring of context menus
[url=http://bespin.org/~draugath/vo/mdi/vo-mdi_alpha2.zip]alpha version 2[/url] (ae918801; does not require DevKit)


]]